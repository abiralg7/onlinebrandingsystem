﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="OnilneBrandingSystem.pages.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--Banner Part Starts Here-->
<div class="main-content">
			<div class="slideshow-container">
			  <div class="mySlides fade" style="display: block;">
			    <div class="numbertext">1 / 3</div>
			    <img runat="server" src="~/images/digital-media.jpg">
			    <div class="text"><h1>Lorem ipsum dolor sit</h1>
			    	<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem exercitationem a quam officiis atque neque doloremque eum est vero aperiam, repellat voluptatibus. Nemo facere officiis fuga aliquam. Quidem, corrupti illum. </p>
			    </div>
			  </div>
			  <div class="mySlides fade" style="display: none;">
			    <div class="numbertext">2 / 3</div>
			    <img runat="server" src="~/images/digital-media.jpg">
			    <div class="text"><h1>Lorem ipsum dolor sit</h1>
			    	<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem exercitationem a quam officiis atque neque doloremque eum est vero aperiam, repellat voluptatibus. Nemo facere officiis fuga aliquam. Quidem, corrupti illum.</p>
			    </div>
			  </div>

			  <div class="mySlides fade" style="display: none;">
			    <div class="numbertext">3 / 3</div>
			    <img runat="server" src="~/images/digital-media.jpg">
			    <div class="text"><h1>Lorem ipsum dolor sit</h1>
			    	<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem exercitationem a quam officiis atque neque doloremque eum est vero aperiam, repellat voluptatibus. Nemo facere officiis fuga aliquam. Quidem, corrupti illum.</p>
			    </div>
			  </div>
			  <a class="prev" onclick="plusSlides(-1)">❮</a>
			  <a class="next" onclick="plusSlides(1)">❯</a>
			</div>
			<br>

			<div style="text-align:center">
			  <span class="dot active" onclick="currentSlide(1)"></span> 
			  <span class="dot" onclick="currentSlide(2)"></span> 
			  <span class="dot" onclick="currentSlide(3)"></span> 
			</div>
		</div>
    <div class="clearfix"></div>
        <!--Banner Part Ends Here-->
        
        <!--Services Starts Here-->
            <div class="services">
                <div class="wrapper">
                    <asp:Label ID="lblTest" runat="server" Text=""></asp:Label>
                    <h2>OUR SERVICES</h2>
                    
                    <div class="service-box">
                        <i class="fa fa-money" aria-hidden="true"></i>
                        <h3>MONETIZE</h3>
                        Description
                    </div>
                    
                    <div class="service-box">
                        <i class="fa fa-address-card-o" aria-hidden="true"></i>
                        <h3>IDENTITY</h3>
                        Description
                    </div>
                    
                    <div class="service-box">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                        <h3>TRAFFIC</h3>
                        Description
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
    <div class="clearfix"></div>
        <!--Services Ends Here-->
        
        <!--Our Customers Starts Here-->
        <div class="brands">
            <div class="wrapper">
                <!--Navigation Starts Here-->
                <div class="categories">
                    <strong>Categories</strong>
                    <ul>
                        <a href="~/Pages/BrandsByCategory.aspx?cat=E-Commerce" runat="server">E-Commerce<li></li></a>
                        <a href="~/Pages/BrandsByCategory.aspx?cat=Travels" runat="server">Travels<li></li></a>
                        <a href="~/Pages/BrandsByCategory.aspx?cat=Clothing" runat="server">Clothing<li></li></a>
                        <a href="~/Pages/BrandsByCategory.aspx?cat=Gadgets" runat="server">Gadgets<li></li></a>
                        <a href="~/Pages/BrandsByCategory.aspx?cat=Furniture" runat="server">Furniture<li></li></a>
                        <a href="~/Pages/BrandsByCategory.aspx?cat=Consultancy" runat="server">Consultancy<li></li></a>
                    </ul>
                </div>
                
                <div class="search">
                    <strong> &nbsp;</strong>
                    <form id="form1" runat="server">
                        <asp:TextBox ID="txtSearch" runat="server" style="width: 70%;border: 2px solid rgb(245,124,33);">

                        </asp:TextBox><asp:Button ID="btnSearch" runat="server" Text="SEARCH" style="padding: 2%; " OnClick="btnSearch_Click" />
                    </form>
                </div>
                <div class="clearfix"></div>
                <!--Navigation Ends Here-->
                <div class="cat-title">Recent BRANDS</div>
                <asp:Label ID="lblNewBrands" runat="server" Text=""></asp:Label>
                
                <div class="clearfix"></div>
                <div class="cat-title">POPULAR BRANDS</div>
                <asp:Label ID="lblPopularBrands" runat="server" Text=""></asp:Label>
                
                <div class="clearfix"></div>
            </div>
        </div>
        <!--Our Customers Starts Here-->
    </asp:Content>